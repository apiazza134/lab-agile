Installation
************

This project is not yet hosted on `pypi`, so you need to install it manually like so::

  $ git clone https://gitlab.com/apiazza134/lab-agile.git
  $ cd lab-agile
  $ python3 setup.py install

Add `sudo` before the last command if necessary, namely when you want to install globally, although we give the following

.. hint:: We recommend working inside a `virtualenv`_; you can create one by executing::

     $ virtualenv -p python3 <name>

   And then activate/deactivate by executing::

     $ source <name>/bin/activate
     ... stuff ...
     $ deactivate

.. _virtualenv: https://virtualenv.pypa.io/en/stable/
