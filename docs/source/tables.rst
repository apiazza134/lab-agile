Building tables
***************

Suppose you want to elaborate a lot of data which is stored in a tabular form. It is not convenient to put it in your `data.yml` file, but you can store it like this e.g. in a `txt` file:

.. literalinclude:: ../sample/table.txt

In order to import such data the function :func:`lab.utils.loadtxtSorted` may come in handy:

.. testcode::

   from lab import loadtxtSorted

   Vin, dVin, Vout, dVout = loadtxtSorted('sample/table.txt')

You can then recast these as `uarray` objects and perform the elaboration:

.. testcode::

   from uncertainties import unumpy as unp

   Vin = unp.uarray(Vin, dVin)
   Vout = unp.uarray(Vout, dVout)

   # sample elaboration
   A = Vout/Vin

Now create a :class:`lab.Table` object and add columns to it:

.. testcode::

   from lab import Table

   tab = Table()
   tab.pushColumn('V\\ped{in}', 'volt', Vin)
   tab.pushColumn('V\\ped{out}', 'volt', Vout)
   tab.pushColumn('A', None, A)

Note that dollars must not appear in the label, as they are automatically added.
Finally, save the table to the data tree:

.. testcode::

   from lab import Data

   d = Data(yamlfile='sample/data.yml')
   d.pushTable(tab, '1.a', 'tab')
   d.toyaml()

The final `yaml` file will contain a :math:`\LaTeX` `tabular` environment which you can directly import in your document (see :ref:`Tables in LuaLaTeX <lualatex-tables>`).

.. testcode::
   :hide:

   from filecmp import cmp
   assert(cmp('elab_data.yml', 'sample/test/elab_data_tables.yml', shallow=False))
