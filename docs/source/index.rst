
Welcome to Lab agile's documentation!
=====================================

`Lab agile` has been written as a toolkit for practical laboratory sessions part of some courses for bachelor's degree in Physics.

This package helps importing experimental data from a `yaml` file (which is easily human readable) as a python `ufloat` object, and exporting it in already formatted :math:`\LaTeX` code (which can be imported in the laboratory report, e.g. by some `Lua` code) after the elaboration has been performed in a python script.

This way, the build process of the final document can be completely automatized, and it's never necessary to manually copy data nor to take care of uncertainties and significant figures, as this is automatically carried out by `Lab agile`.

The source code is hosted on `GitLab`_. If you find any bugs, please report them by `creating an issue`_ or by contacting the authors `Alessandro Piazza`_ and `Marco Venuti`_.

You can browse the coverage report `here`_.

.. _GitLab: https://gitlab.com/apiazza134/lab-agile
.. _creating an issue: https://gitlab.com/apiazza134/lab-agile/-/issues
.. _Alessandro Piazza: https://uz.sns.it/~alepiazza
.. _Marco Venuti: https://uz.sns.it/~marco_venuti
.. _here: coverage/index.html

.. toctree::
   :maxdepth: 2
   :caption: Contents

   installation
   basic_usage
   tables
   lualatex
   reference

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
