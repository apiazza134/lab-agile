Basic Usage
***********

Raw data from the experiment must be saved in a ``data.yml`` file like this:

.. literalinclude:: ../sample/data.yml
   :language: yaml

.. attention:: Entries which are not in the format as a list of ``[float, float, str]`` will not be interpeted as a measurements, won't be parsed and will be left unchanged in the final `yaml` output. This might be a wanted behaviour if you want to add additional informations (such as metadata), but malformed measurements will be ignored (for instance if you forget the error or the unit).

You can then import :class:`lab.Data`, which is going to contain the whole data tree for the analysis.

.. testcode::

   from lab import Data
   # Note that yamlfile defaults to './data.yml'
   d = Data(yamlfile='sample/data.yml')

In order to extract an ``ufloat`` corresponding to a measurement, you can use :meth:`lab.Data.get` like this:

.. testcode::

   voltage = d.get('1.a', 'VCC')
   print(f'VCC = {voltage} V')

Which will result in

.. testoutput::

   VCC = 4.980+/-0.030 V

You can perform arbitrary arithmetic involving ``ufloat``:

.. testcode::

   transitionTime = d.get('1.b', 'LH', 'tQ50') - d.get('1.b', 'LH', 'tD50')
   print(f'transition time = {transitionTime} s')

.. testoutput::

   transition time = (9.0+/-1.0)e-09 s

At the end, if you want to export the elaborated data in :math:`\LaTeX` code (to include in your laboratory report, for instance), you can invoke :meth:`lab.Data.push` and :meth:`lab.Data.toyaml`:

.. testcode::

   d.push(transitionTime, 'second', '1.a', 'LH', 'transitionTime')
   d.toyaml()

This will produce a file called `elab_data.yml` containing :math:`\LaTeX` code where measurement are formatted as siunitx_ strings:

.. literalinclude:: ../sample/test/elab_data_basic_usage.yml
   :language: yaml

This can then be imported in a document, for example using :math:`\text{Lua}\LaTeX`. See :doc:`lualatex` for more.

.. _siunitx: https://ctan.org/pkg/siunitx

.. testcode::
   :hide:

   from filecmp import cmp
   assert(cmp('elab_data.yml', 'sample/test/elab_data_basic_usage.yml', shallow=False))
