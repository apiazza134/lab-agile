Integration with `LuaLaTeX`
****************************

Lua code can be used inside :math:`\LaTeX` in order to include the output of the data elaboration in your document.

First of all of course you need :math:`\text{Lua}\LaTeX` (the code is tested in the version included in `texlive` 2019). The following dependencies are also required:

- ``lua5.3``
- ``lua-yaml``

We now need to tell :math:`\text{Lua}\LaTeX` where to look for Lua's libraries. Assuming your :math:`\LaTeX` installation is in, e.g., ``/usr/local/texlive/2019/``, you need to append the following lines to your ``/usr/local/texlive/2019/texmf.cnf``::

  LUAINPUTS.lualatex = /usr/share/lua/5.3//;/usr/local/share/lua/5.3/*//;/usr/local/lib/lua/5.3//;/usr/local/lib/lua/5.3/*//;/usr/share/lua/5.3//;/usr/share/lua/5.3/*//;.//;$TEXMFDOTDIR;$TEXMF/scripts/{$progname,$engine,}/{lua,}//;$TEXMF/tex/{lualatex,latex,luatex,generic,}//

  CLUAINPUTS = /usr/local/lib/lua/5.3//;/usr/lib/x86_64-linux-gnu/lua/5.3//;/usr/lib/lua/5.3//;/usr/local/lib/lua/5.3//;.//;$TEXMFDOTDIR;$SELFAUTOLOC/lib/{$progname,$engine,}/lua//

In order to load the content of a `.yml` file into a :math:`\LaTeX` document you can define the following Lua function (e.g. in your custom `.sty` file if you have one)

.. code-block:: latex

   \begin{luacode*}
       function parseYaml(path)
          local lyaml = require("lyaml")

          local file = io.open(path, "r")
          local out = lyaml.load(file:read("*all"))
          io.close(file)

          return out
       end
   \end{luacode*}

Finally, call this in your document's preamble

.. code-block:: latex

   \directlua{
       data = parseYaml("./elab_data.yml")
   }

And the inside your ``document`` you can access ``data`` as a normal Lua table:

.. code-block:: latex

    \[ V\ped{CC} = \directlua{tex.print(data['1.a']['VCC'])} \]


.. _lualatex-tables:

Tables
======

For tables we need to strip newlines from the content of the `yaml`: this is neded becuse tables in the `yaml` file are `literal` blocks (i.e they start with the ``|-`` character) to make them human readable, but the ``tex.print`` function apparently translate the ``\n`` in annoying dots. You can add the following Lua function

.. code-block:: latex

   \begin{luacode*}
       function texprintStripNL(text)
           local text, count = string.gsub(text, '\n', '')
           tex.print(text)
       end
   \end{luacode*}

Then you can input the table in your document as

.. code-block:: latex

   \begin{table}[h]
       \centering
       \directlua{texprintStripNL(data['table'])}
       \caption{Very cusu table}
   \end{table}

.. hint:: You can compile this with ``lualatex <filename>.tex``, although we recommend using ``latexmk``, which compiles the document the right number of times in order to get your references right::

   $ latexmk -lualatex -pdf <filename>.tex
