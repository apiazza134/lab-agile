#!/usr/bin/env python

from unittest import TestCase, main

import lab.si as si

from uncertainties import ufloat


class SiTest(TestCase):
    help = "Test per formattazione di float e ufloat in latex con siunitx"

    def test_float(self):
        """Verifico la fomattazione dei float con e senza unità di misura
        """
        value = 1.5567e-8
        precision = 2

        latex_without_unit = si.siFormat(
            value=value,
            precision=precision,
            unit=None
        )
        self.assertEqual(latex_without_unit, "\\num{ 15.57e-9 }")

        latex_with_unit = si.siFormat(
            value=value,
            unit='ampere',
            precision=precision
        )
        self.assertEqual(latex_with_unit, "\\SI[]{ 15.57 }{ \\nano\\ampere }")

    def test_float_outofbound(self):
        """Verifico la fomattazione dei float con unità senza prefisso
        """
        value = 1.5567812e20
        precision = 2
        latex = si.siFormat(
            value=value,
            unit='ampere',
            precision=precision
        )
        self.assertEqual(latex, "\\SI[]{ 155.68e18 }{ \\ampere }")

    def test_ufloat(self):
        """Verifico la fomattazione degli ufloat con e senza unità di misura
        """
        value = ufloat(1.5567e8, 0.027e8)
        precision = 2

        latex_without_unit = si.siFormat(
            value=value,
            precision=precision,
            unit=None
        )
        self.assertEqual(latex_without_unit, "\\num{ 155.7+-2.7e6 }")

        latex_with_unit = si.siFormat(
            value=value,
            unit='ampere',
            precision=precision
        )
        self.assertEqual(latex_with_unit, "\\SI[]{ 155.7+-2.7 }{ \\mega\\ampere }")

    def test_ufloat_outofbound(self):
        """Verifico la fomattazione degli ufloat con unità senza prefisso
        """
        value = ufloat(1.5567812e-20, 0.2e-20)
        precision = 1
        latex = si.siFormat(
            value=value,
            unit='ampere',
            precision=precision
        )
        self.assertEqual(latex, "\\SI[]{ 16+-2e-21 }{ \\ampere }")


if __name__ == "__main__":
    main()
