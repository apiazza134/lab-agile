#!/usr/bin/env python

from unittest import TestCase, main

from lab.data import Data
from lab.utils import loadtxtSorted
from lab.table import Table

from uncertainties import unumpy as unp
import yaml


class TablesTest(TestCase):

    def test_data_load(self):
        dataYaml = "fixtures/data.yml"
        # pickle = "elab_data.pkl"
        # outYaml = "elab_data.yml"

        Vin, dVin, Vout, dVout = loadtxtSorted('fixtures/table.txt')

        Vin = unp.uarray(Vin, dVin)
        Vout = unp.uarray(Vout, dVout)

        t = Table()
        t.pushColumn('V\\ped{in}', 'volt', Vin)
        t.pushColumn('V\\ped{out}', 'volt', Vout)

        d = Data(yamlfile=dataYaml)

        d.elab_data['cusumano'] = t

        d.save()
        d.toyaml()

        correctOutput = '\\begin{tabular}{ cc } \\toprule\n$ V\\ped{in}\\ [\\si{ \\volt }] $ & $ V\\ped{out}\\ [\\si{ \\volt }] $ \\\\ \\midrule\n\\SI[multi-part-units=single]{ 600+-100 }{ \\micro\\nothing } & \\SI[multi-part-units=single]{ 4.40+-0.02 }{ \\nothing } \\\\\n\\SI[multi-part-units=single]{ 173.6+-0.8 }{ \\milli\\nothing } & \\SI[multi-part-units=single]{ 4.33+-0.02 }{ \\nothing } \\\\\n\\SI[multi-part-units=single]{ 627+-3 }{ \\milli\\nothing } & \\SI[multi-part-units=single]{ 4.04+-0.02 }{ \\nothing } \\\\\n\\SI[multi-part-units=single]{ 1.246+-0.006 }{ \\nothing } & \\SI[multi-part-units=single]{ 172.6+-0.8 }{ \\milli\\nothing }\\\\ \\bottomrule\n\\end{tabular}'

        with open('elab_data.yml') as f:
            self.assertEqual(
                yaml.load(f, Loader=yaml.SafeLoader)['cusumano'],
                correctOutput
            )


if __name__ == "__main__":
    main()
