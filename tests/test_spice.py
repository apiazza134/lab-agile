#!/usr/bin/env python3

from unittest import TestCase, main
from lab.spice import parse_spice
from math import isclose


class DataTest(TestCase):
    def test_data_load(self):
        data = parse_spice('fixtures/spice.txt')

        self.assertEqual(len(data[0]), 14)
        self.assertTrue(
            all(map(isclose, data.transpose()[-1], [1.34896288259165e-01, 5.73584731735846e+01, 1.79999247378467e+02]))
        )


if __name__ == "__main__":
    main()
