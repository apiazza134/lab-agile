#!/usr/bin/env python3

from unittest import TestCase, main

from lab.data import Data
from lab.measure import Measure

from numpy import pi


class DataTest(TestCase):

    def test_data_load(self):
        yaml = "./fixtures/data.yml"
        pickle = "./fixtures/elab_data.pkl"
        outyaml = "./fixtures/elab_data.yml"

        d = Data(yamlfile=yaml, picklefile=pickle)
        data = d.elab_data['1.a']

        R = data['R'].value
        C = data['C'].value
        f = 1/(2*pi*R*C)

        data['f'] = Measure(f, 'hertz')

        d.save()
        d.toyaml(outYaml=outyaml)


if __name__ == "__main__":
    main()
