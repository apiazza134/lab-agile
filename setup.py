from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name="lab-agile",
    version="0.0.1",
    description="Utilità per le relazioni di laboratorio",
    long_description=long_description,
    long_description_content_type='text/markdown',
    url="https://gitlab.com/apiazza134/lab-agile",
    author="Alessandro Piazza, Marco Venuti",
    author_email="alessandro.piazza@sns.it, marco.venuti@sns.it",
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: GPLv3',
        'Programming Language :: Python :: 3.7',
    ],
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    python_requires='>=3.7.6',
    install_requires=[
        'numpy',
        'pyyaml',
        'uncertainties'
    ],
    extras_require={
        'test': ['coverage'],
    },
    project_urls={
        'Bug Reports': 'https://gitlab.com/apiazza134/lab-agile/issues',
        'Source': 'https://gitlab.com/apiazza134/lab-agile',
    }
)
