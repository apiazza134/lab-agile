import numpy
import re


def parse_spice(path: str) -> numpy.array:
    """Parse data exported from SPICE simulations."""
    toReturn = []
    with open(path, encoding='latin_1') as inFile:
        next(inFile)
        for line in inFile:
            match = re.findall(r'[0-9\.e+-]+', line)
            if match:
                toReturn.append(numpy.array(list(map(float, match))))
    if len(toReturn) > 0:
        return numpy.array(toReturn).transpose()
    else:
        return None
