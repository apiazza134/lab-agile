from numpy import ndarray, int64
from uncertainties import core, unumpy
import re

prefixes = {
    # piccoli
    -3: '\\milli',
    -6: '\\micro',
    -9: '\\nano',
    -12: '\\pico',
    -15: '\\femto',
    0: '',
    3: '\\kilo',
    6: '\\mega',
    9: '\\giga',
    12: '\\tera',
    15: '\\peta'
}

ufloat_class = (core.Variable, core.AffineScalarFunc)


def shift_value(value, exp: int, offset: int):
    """Multiply value to have exponent multiple of 3 (+ offset)
    """
    prefix_exp = exp - (exp % 3) + offset
    return prefix_exp, value * 10**(-prefix_exp)


def siFormat(value, unit: str, precision: int = 1, offset: int = 0, siargs: str = '') -> str:
    """Format floats and ufloat (and int) with or without units to latex code for
    the siunitx package
    """
    if isinstance(value, int) or isinstance(value, int64):
        return siFormat(float(value), unit, precision, offset, siargs)

    elif isinstance(value, float):
        string = f"{value:.{precision}e}"
        str_value, str_exp = re.match(r"(.*)e(.*)", string).groups()

        prefix_exp, value = shift_value(value, int(str_exp), offset)

        str_value = f"{value:.{precision}f}"
        str_err = ''

    elif isinstance(value, ufloat_class):
        ustring = f"{value:.{precision}ue}"
        str_value, str_exp = re.match(r"\((.*)\)e(.*)", ustring).groups()

        prefix_exp, value = shift_value(value, int(str_exp), offset)

        ustring = f"{value:.{precision}uf}"
        str_value, str_err = re.match(r"(.*)\+/-(.*)", ustring).groups()
        str_err = f"+-{str_err}"

    else:
        raise TypeError("Value argument must be either int, numpy.int64, float or ufloat")

    str_exp = f"e{str(prefix_exp)}"

    if unit is None:
        return f"\\num{{ {str_value}{str_err}{str_exp} }}"
    else:
        if prefix_exp in prefixes:
            unit_prefix = prefixes[prefix_exp]
            str_exp = ''
        else:
            unit_prefix = ''
        return f"\\SI[{siargs}]{{ {str_value}{str_err}{str_exp} }}{{ {unit_prefix}\\{unit} }}"


def floatSiFormat(value, unit: str, precision: int = 1, offset: int = 0, siargs: str = '') -> str:
    return siFormat(float(value), unit, precision, offset, siargs)


def ufloatSiFormat(value, unit: str, precision: int = 1, offset: int = 0, siargs: str = '') -> str:
    return siFormat(float(value), unit, precision, offset, siargs)


def arraySiFormat(array, unit: str) -> [str]:
    if unit is not None:
        unit = 'nothing'
    return [siFormat(value, unit, siargs='multi-part-units=single') for value in array]


def uarraySiFormat(array: unumpy.uarray, unit: str) -> [str]:
    """DEPRECATED"""
    if unit is not None:
        unit = 'nothing'
    return [siFormat(value, unit, siargs='multi-part-units=single') for value in array]
