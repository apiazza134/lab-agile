import numpy
from uncertainties import ufloat
import csv


"""
Tektronix TDS 1012
"""


def measureMeanUncertainty(measure: float, div: float) -> ufloat:
    """Returns an ufloat corresponding to a voltage measure
    in mean mode.
    Arguments must be passed in SI units (e.g. 1mV -> 1e-3 V)."""
    return ufloat(
        measure,
        numpy.sqrt(
            (0.03*measure)**2 +
            (0.1*div)**2 +
            (1e-3)**2
        )
    )


def measureMeanUncertaintyArray(measure: [float], div: [float]) -> numpy.array:
    """Like :func:`measureMeanUncertainty`, but mapped onto lists.
    """
    return numpy.array(list(map(measureMeanUncertainty, list(measure), list(div))))


def readCSV(path: str, cut: int = 17) -> numpy.array:
    """Returns ``.csv`` data acquired by the oscilloscope as a numpy array.
    """
    with open(path) as f:
        reader = csv.reader(f, delimiter=',')
        return numpy.array(
            [list(filter(lambda x: x, row)) for row in reader][cut:],
            dtype=float
        ).transpose()
