from uncertainties import core

from .si import siFormat

ufloat_class = (core.Variable, core.AffineScalarFunc)


class Measure():
    """A wrapper around ufloat, aware of unit, precision, offset.

    Attributes:
        value: the value of the measurement which must be either ``int``,
            ``float`` or ``ufloat``
        unit: the unit of the measurement which must be ``siunitx``-compatible
        precision: the number significant digits of the measurement
        offset: formatting offset used in the :func:`lab.Measure.get` method
    """
    def __init__(self, value, unit: str, precision: int = 1, offset: int = 0):
        if isinstance(value, (int, float, *ufloat_class)):
            self.value = value
        else:
            raise TypeError(
                "Measure.value must be either int, float or ufloat"
            )
        self.unit = unit
        self.precision = precision
        self.offset = offset

    def __eq__(self, other) -> bool:
        """Define equality between Measure objects as equality attribute-wise.
        A check by hand is performed between the value attribute
        between the two measures: this is necessary sice formally if
        ``X = ufloat(x, dx)`` and ``Y = ufloat(x, dx)`` then not
        necessarely X == Y beacuse conceptually they are different
        measurements.

        """
        if isinstance(other, Measure) is False:
            return NotImplemented

        if isinstance(self.value, ufloat_class) and isinstance(other.value, ufloat_class):
            value_eq = all((
                self.value.nominal_value == other.value.nominal_value,
                self.value.std_dev == other.value.std_dev
            ))
        elif isinstance(self.value, (int, float)) and isinstance(other.value, (int, float)):
            value_eq = (self.value == other.value)
        else:
            return NotImplemented

        return all((
            value_eq,
            self.unit == other.unit,
            self.precision == other.precision,
            self.offset == other.offset
        ))

    def __ne__(self, other) -> bool:
        return not self.__eq__(other)

    def __hash__(self):
        return hash((self.value, self.unit, self.precision, self.offset))

    def get(self):
        """
        Returns:
            the value attribute.
        """
        return self.value

    def toSiunitx(self) -> str:
        """Wrapper around :func:`lab.si.siFormat`
        Returns:
            Measure object formatted to be ``siunitx``-compatible
        """
        return siFormat(
            value=self.value,
            unit=self.unit,
            precision=self.precision,
            offset=self.offset
        )
