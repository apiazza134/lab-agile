import pickle
import yaml
import re
from copy import deepcopy
from functools import reduce
from operator import getitem

from uncertainties import ufloat

from .merge import merge_dicts
from .measure import Measure
from .table import Table


class Data:
    def __init__(self, elab_data: dict = None, yamlfile: str = "./data.yml", picklefile: str = "./elab_data.pkl"):
        self.data = self.loadYaml(yamlfile)

        if elab_data is None:
            self.elab_data = self.loadPickle(picklefile)
        else:
            self.elab_data = elab_data

        self.elab_data = merge_dicts(self.data, self.elab_data)
        self.dumpPickle(self.elab_data, picklefile)

    def loadYaml(self, yamlfile: str) -> dict:
        try:
            with open(yamlfile, "r") as yamlFile:
                yamldict = yaml.load(yamlFile, Loader)
                return self.yamlToDictOfMeasures(yamldict)
        except FileNotFoundError:
            print("Yaml file does not exist")
            return None

    def isMeasureCompatible(self, entry: list) -> bool:
        if len(entry) == 3:
            return all((
                isinstance(entry[0], (int, float)),  # value
                isinstance(entry[1], (int, float)),  # error
                isinstance(entry[2], str)            # unit
            ))
        else:
            return False

    def yamlToDictOfMeasures(self, yamldict: dict) -> dict:
        toparse = deepcopy(yamldict)
        toreturn = dict()

        for key, value in toparse.items():
            if isinstance(value, dict):
                toreturn[key] = self.yamlToDictOfMeasures(value)
            elif isinstance(value, list):
                if self.isMeasureCompatible(value) is True:
                    toreturn[key] = Measure(
                        value=ufloat(value[0], value[1]),
                        unit=value[2]
                    )
                else:
                    toreturn[key] = value
            else:
                toreturn[key] = value

        return toreturn

    def loadPickle(self, picklefile: str) -> dict:
        try:
            with open(picklefile, "rb") as pickleFile:
                return pickle.load(pickleFile)
        except FileNotFoundError:
            return None

    def dumpPickle(self, todump: dict, picklefile: str):
        with open(picklefile, "wb+") as pickleFile:
            pickle.dump(todump, pickleFile)

    def get(self, *args: str) -> ufloat:
        """Calls the :func:`lab.Measure.get` method of a target element in
        the data tree

        Args:
            *args: keywords of the value you want to get, interpreted as
                orderd from the outermost to the innermost ones.

        Returns:
            what :func:`lab.Measure.get` returns.
        """
        entry = reduce(getitem, args, self.elab_data)
        if isinstance(entry, Measure):
            return entry.get()
        else:
            raise AttributeError(
                f"elab_data[{ ']['.join(args) }] is not a Measure object"
            )

    def recursivePush(self, obj: any, *args: str):
        """Insert an arbitrary object into the data tree in a recursive manner

        Args:
            obj: the object to be inserted
            *args: keywords of the object to be inserted, interpreted as
                orderd from the outermost to the innermost ones.
        """
        topush = reduce(lambda x, y: {y: x}, reversed(args), obj)
        self.elab_data = merge_dicts(self.elab_data, topush)

    def push(self, value, unit: str, *args: str, precision: int = 1, offset: int = 0):
        """Pushes an ufloat into the data tree

        Args:
            value: the measurement to be pushed.
            unit: the unit expressed as a ``siunitx``-compatible string.
            precision: significant figures.
            offset: multiplicative factor.
            *args: keywords of the measurement to be pushed, interpreted as
                orderd from the outermost to the innermost ones.
        """

        meas = Measure(value=value, unit=unit,
                       precision=precision, offset=offset)
        self.recursivePush(meas, *args)

    def pushTable(self, table: Table, *args):
        """Pushes a :class:`lab.Table` object into the data tree. This
        function is needed for backward compatibility and is just a call to
        :func:`lab.Data.recursivePush` method.

        Args:
            table: the table to be inserted
            *args: keywords of the table to be inserted, interpreted as
                orderd from the outermost to the innermost ones.
        """
        self.recursivePush(table, *args)

    def save(self, picklefile: str = "./elab_data.pkl", overwrite: bool = True):
        """Save the data tree to a picke file.
        """
        old = self.loadPickle(picklefile)
        if overwrite is True:
            todump = merge_dicts(self.elab_data, old)
        else:
            todump = merge_dicts(old, self.elab_data)

        self.dumpPickle(todump, picklefile)

    def toLaTeX(self, toparse: dict) -> dict:
        toreturn = dict()
        for key, value in toparse.items():
            if isinstance(value, dict):
                toreturn[key] = self.toLaTeX(toparse[key])
            elif isinstance(value, Measure):
                toreturn[key] = value.toSiunitx()
            elif isinstance(value, Table):
                toreturn[key] = literal(value.toLaTeX())
            else:
                toreturn[key] = value
        return toreturn

    def toyaml(self, outYaml: str = "./elab_data.yml"):
        """Exports the contents of the data tree to a yaml file containing LaTeX code.
        """
        # Per cusmanare le stringhe multilinea
        yaml.add_representer(literal, literal_presenter)

        with open(outYaml, "w+") as yamlFile:
            yaml.dump(self.toLaTeX(self.elab_data), yamlFile)


class Loader(yaml.SafeLoader):
    """ Customized yaml.Loader to load floats properly
    """
    def __init__(self, stream):
        super().__init__(stream)
        self.add_implicit_resolver(
            u'tag:yaml.org,2002:float',
            re.compile(u'''^(?:
            [-+]?(?:[0-9][0-9_]*)\\.[0-9_]*(?:[eE][-+]?[0-9]+)?
            |[-+]?(?:[0-9][0-9_]*)(?:[eE][-+]?[0-9]+)
            |\\.[0-9_]+(?:[eE][-+][0-9]+)?
            |[-+]?[0-9][0-9_]*(?::[0-5]?[0-9])+\\self.[0-9_]*
            |[-+]?\\.(?:inf|Inf|INF)
            |\\.(?:nan|NaN|NAN))$''', re.X),
            list(u'-+0123456789.')
        )


class literal(str):
    pass


def literal_presenter(dumper, data):
    return dumper.represent_scalar('tag:yaml.org,2002:str', data, style='|')
