from copy import deepcopy


def merge_dicts(dictUpper: dict, dictLower: dict) -> dict:
    """Merge two dictionaries recursively: dictUpper values have priority over
    dictLower values.
    """
    dictUpper = deepcopy(dictUpper)
    dictLower = deepcopy(dictLower)

    if dictUpper is None:
        return dictLower
    if dictLower is None:
        return dictUpper

    for key, value in dictLower.items():
        if key in dictUpper:
            if isinstance(value, dict) and isinstance(dictUpper[key], dict):
                dictUpper[key] = merge_dicts(dictUpper[key], value)
            elif value != dictUpper[key]:
                print(f"{key}:{value} already exists in the target dictionary.")

        else:
            dictUpper[key] = value

    return dictUpper
