import os
import pickle
import yaml
from uncertainties import ufloat
from copy import deepcopy

from .table import Table


class LocalData:
    def __init__(self, section: str, yamlfile: str = "./data.yml", pkl: str = "./elab_data.pkl"):
        self.section = section
        self.data = dict()
        self.elab_data = dict()

        # Load data
        if os.path.exists(pkl) is False:
            with open(yamlfile, "r") as yamlFile:
                self.data = yaml.load(yamlFile, loader)
                if self.section in self.data.keys():
                    self.data = self.data[self.section]
                    # Write in elab_data already exisiting measurements and everythig else
                    for key in self.data.keys():
                        if len(self.data[key]) == 3:
                            self.push(ufloat(self.data[key][0],
                                             self.data[key][1]),
                                      self.data[key][2], key)
                        else:
                            self.elab_data[key] = deepcopy(self.data[key])

            with open(pkl, "wb+") as pickleFile:
                pickle.dump({self.section: self.elab_data}, pickleFile)

        with open(pkl, "rb") as pickleFile:
            self.elab_data = pickle.load(pickleFile)
            if self.section in self.elab_data.keys():
                self.elab_data = self.elab_data[self.section]
            else:
                self.elab_data = dict()

    def get(self, key: str) -> ufloat:
        return self.elab_data[key]["value"]

    def push(self, value: ufloat, unit: str, key: str,
             precision: int = 1, offset: int = 0):
        self.elab_data[key] = {
            "value": value,
            "unit": unit,
            "precision": precision,
            "offset": offset
        }

    def pushTable(self, tab: Table, key: str):
        self.elab_data[key] = tab.toTabular()

    def save(self, outpkl: str = "./elab_data.pkl", overwrite: bool = False):
        try:
            with open(outpkl, "rb") as pickleFile:
                old = pickle.load(pickleFile)
        except FileNotFoundError:
            old = None

        if old is None:
            todump = {self.section: self.elab_data}
        else:
            todump = deepcopy(old)
            if self.section in old.keys():
                for key in self.elab_data.keys():
                    if key in old[self.section].keys() and overwrite is False:
                        print(f"{self.section}:{key} already exists in {outpkl}")
                    else:
                        todump[self.section][key] = self.elab_data[key]
            else:
                todump[self.section] = self.elab_data

        with open(outpkl, "wb+") as pickleFile:
            pickle.dump(todump, pickleFile)
