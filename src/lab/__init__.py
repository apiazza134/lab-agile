from .data import *
from .table import *
from .utils import *
from .oscilloscope import *

name = "lab"
