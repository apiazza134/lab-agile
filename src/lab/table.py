from .si import arraySiFormat
import numpy as np


class Table:
    """A high-level object representing a LaTeX table.

    Attributes:
        columns: list of the columns of the table. The first element is
            expected to be the column heading, while the remaining is supposed
            to an ``int``, a ``float`` or a ``ufloat``.
    """
    def __init__(self):
        self.columns = []

    def __eq__(self, other):
        """Define equality between Table objects attribute-wise.
        """
        if not isinstance(other, Table):
            return NotImplemented

        return all([
            i['label'] == j['label'] and i['unit'] == j['unit'] and (i['data'] == j['data']).all()
            for i, j in zip(self.columns, other.columns)
        ])

    def __ne__(self, other):
        return not self.__eq__(other)

    def pushColumn(self, label: str, unit: str, data):
        """Adds a new column to table.

        Args:
            label (str): the heading which will appear on the top of the column.
            unit (str): the unit which will appear next to the label. This can be ``None``.
            data: the data itself as a list.
        """
        self.columns.append(
            {
                'label': label,
                'unit': unit,
                'data': np.array(data)
            }
        )

    def toLaTeX(self) -> str:
        """Outputs the Table object as a LaTeX tabular envirnoment.
        """
        tex_columns = []
        for c in self.columns:
            unit_str = f"\\ [\\si{{ \\{c['unit']} }}]" if c['unit'] is not None else ''
            tex_columns.append(
                [f"$ {c['label']}{unit_str} $"] + arraySiFormat(c['data'], c['unit'])
            )

        rows = list(zip(*tex_columns))
        ncols = len(tex_columns)
        toreturn = f"\\begin{{tabular}}{{ {ncols*'c'} }} \\toprule\n"
        toreturn += " & ".join(rows[0]) + " \\\\ \\midrule\n"  # heading
        tmp = [" & ".join(row) for row in rows[1:]]
        toreturn += " \\\\\n".join(tmp) + "\\\\ \\bottomrule\n\\end{tabular}"

        return toreturn

    def sortRows(self, keys: tuple = (0,), reverse=False):
        """Sort rows lexically according to the columns specified in keys (first column is 0)
        Priority decreases left to right.
        """
        tosort = np.array([
            c['data']
            for c in self.columns
        ])

        ind = np.lexsort([tosort[i] for i in reversed(keys)])

        for n, c in enumerate(self.columns):
            c['data'] = reversed(tosort[n][ind]) if reverse else tosort[n][ind]
