import numpy as np
import os
from re import search
from glob import glob
from .data import Data


def loadtxtSorted(path: str, column: int = 0):
    """Sintassi un po' criptica basata su
    https://thispointer.com/sorting-2d-numpy-array-by-column-or-row-in-python/
    """
    data = np.loadtxt(path, unpack=True)
    return data[:, data[column].argsort()]


def cov2corr(cov: np.ndarray) -> np.ndarray:
    std_err = np.sqrt(np.diag(cov))
    return cov / np.outer(std_err, std_err)


def degstring_to_rad(deg_string: str) -> float:
    """Converts a string expressing an angle in degrees and primes
    (e.g. "37° 14'") to the corresponding angle in radians
    """
    matches = search(r"(\s*([\d\.\d]+)°)?(\s*([\d\.\d]+)')?", deg_string)
    deg, primes = tuple(map(
        lambda x: 0 if x is None else float(x), matches.group(2, 4)
    ))
    return (deg + primes/60) * (np.pi/180)


def run_chain(scripts: list = []):
    """Removes pre-existing elaborated data, and runs specified scripts in
    sequence, finally exporting the elab_data.yml file."""
    for f in glob('elab_data*'):
        os.remove(f)

    for script in scripts:
        if os.system(f'./{script}'):
            raise RuntimeError(f'Execution of {script} failed.')

    Data().toyaml()


def matrixToTex(mat: np.ndarray, Tmatrix: str = 'pmatrix', precision: int = 2):
    """Convert a 2-dimensional numpy array to LaTeX code
    """
    return(
        f'\\begin{{{Tmatrix}}}' +
        ' \\\\ '.join(
            map(
                lambda row: ' & '.join(map(lambda x: f'{x:.{precision}f}', row)), mat
            )
        ) + f'\\end{{{Tmatrix}}}'
    )
